package vegetables;

/**
 *
 * @author iuabd
 */

import java.util.ArrayList;

public class VegetableSimulation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //instance(s)
        VegetableFactory factory = VegetableFactory.getInstance();
        
        Vegetable carrot1 = factory.getVegetable(VegetableEnum.CARROT, "ORANGE", 2);
        Vegetable carrot2 = factory.getVegetable(VegetableEnum.CARROT, "YELLOW", 1);
        
        Vegetable beet1 = factory.getVegetable(VegetableEnum.BEET, "RED", 2);
        Vegetable beet2 = factory.getVegetable(VegetableEnum.BEET, "ORANGE", 1);
    
        
        System.out.println("The carrot is " + carrot1.getColor() + ", its size is " + carrot1.getSize() + 
                        ", so is it Ripe? " + carrot1.isRipe() +
                            
                            "\nThe carrot is " + carrot2.getColor() + ", its size is " + carrot2.getSize() + 
                        ", so is it Ripe? " + carrot2.isRipe());
        
        System.out.println("The beet is " + beet1.getColor() + ", its size is " + beet1.getSize() + 
                        ", so is it Ripe? " + beet1.isRipe() +
                            
                            "\nThe beet is " + beet2.getColor() + ", its size is " + beet2.getSize() + 
                        ", so is it Ripe? " + beet2.isRipe());
    }
}
