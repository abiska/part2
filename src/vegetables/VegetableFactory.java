package vegetables;

/**
 *
 * @author iuabd
 */
public class VegetableFactory {
    
    private static VegetableFactory factory; //private object
    
    private VegetableFactory(){} //private default constructor
    
    public static VegetableFactory getInstance() //the only public method that allows 1 instanciation at a time
    {
        if(factory==null)
            factory = new VegetableFactory();
        
        return factory;
    }
    
    public Vegetable getVegetable(VegetableEnum type, String color, double size){
    
        switch(type){
                case CARROT: return new Carrot(color, size);
                case BEET: return new Beet(color, size);
        }
        return null;
    
    }   
}
